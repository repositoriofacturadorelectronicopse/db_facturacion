SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Juan Alarcon>
-- Create date: <Create Date,28/03/2021>
-- Description:	<Description>
-- =============================================
CREATE procedure [dbo].[usp_insert_docrefeguia]
(@empresa_id	int,
@documento_id	varchar(15),
@tipodocumento_id	varchar(2),
@tipoguia	varchar(2),
@numeroguia	varchar(20)
)
as
insert into docrefeguia(empresa_id,documento_id,tipodocumento_id,tipo_guia,numero_guia)
values(@empresa_id,@documento_id,@tipodocumento_id,@tipoguia,@numeroguia)

GO
