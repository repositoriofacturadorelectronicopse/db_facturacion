SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Juan Alarcon>
-- Create date: <Create Date,27/03/2021>
-- Description:	<Description>
-- =============================================
CREATE PROCEDURE [dbo].[Usp_listar_docpersona](@codigo varchar(255),@estado varchar(255), @indpersona varchar(255)) as
begin

select codigo, codigosunat, nombre, longitud, tipo, indcontrib, indlongexacta, indpersona, orden, estado from docpersona where 1=1
 

    and codigo= CASE WHEN @codigo != '' THEN @codigo else codigo  END
    and  estado = case when @estado != '' then  @estado else estado END
    and  indpersona = case when @indpersona != '' then  @indpersona else indpersona END
    order by orden asc;
end






GO
