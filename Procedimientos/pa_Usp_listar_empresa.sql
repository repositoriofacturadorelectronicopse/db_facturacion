SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Juan Alarcon
-- Create date: <Create Date,27/03/2021>
-- Description:	<Description>
-- =============================================
CREATE PROCEDURE [dbo].[Usp_listar_empresa](
    @IdEmpresa int, 
    @Nrodocumento varchar(255), 
    @RazonSocial  varchar(255),
    @Estado  varchar(255),
    @Idpuntoventa int) as
begin

select e.*,pv.codpais codpais, pv.coddis coddis,
 d.descripcion departamento, p.descripcion provincia, t.descripcion distrito  from empresa e 
left join (select * from puntoventa pv0 where pv0.id= @Idpuntoventa and pv0.empresa_id=@IdEmpresa) pv on pv.empresa_id=e.id
left join udistritos t on pv.coddis=t.id  
left join uprovincias p on t.idprov=p.id 
left join udepartamentos d on p.iddep=d.id 


where e.borrado=0 
    and e.id = CASE WHEN @IdEmpresa != 0 THEN @IdEmpresa else e.id  END
    and e.nrodocumento like   CASE WHEN @Nrodocumento != '' THEN '%'+@Nrodocumento+'%' else '%'+e.nrodocumento+'%'   END
    and e.razonsocial like CASE WHEN @RazonSocial != '' THEN '%'+@RazonSocial+'%' else '%'+e.razonsocial+'%' END
    and e.estado  = CASE WHEN @Estado != '' THEN  @Estado else e.estado  END

    order by e.razonsocial asc;

end







GO
