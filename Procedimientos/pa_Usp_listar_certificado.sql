SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Juan Alarcon>
-- Create date: <Create Date,27/03/2021>
-- Description:	<Description>
-- =============================================
CREATE PROCEDURE [dbo].[Usp_listar_certificado](@id int,@idEmpresa int, @nombre varchar(255)) as
begin

select id, empresa_id, nombre, ubicacion, username, clave, clavecertificado, idSignature, flagOSE
 from certificado where borrado=0
    and id= CASE WHEN @id !=0 THEN @id else id  END
    and  empresa_id = case when @idEmpresa !=0 then  @idEmpresa else empresa_id END
    and nombre like '%' + @nombre + '%'
    order by nombre asc;
end




GO
