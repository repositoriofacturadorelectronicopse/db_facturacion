-- ----------------------------
--  Procedure definition for `actualizar_facturaxml`
-- ----------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[actualizar_facturaxml](@empresa_id int,@documento_id varchar(15),@tipodocumento_id varchar(2), @nombrexml varchar(100), @valorresumen varchar(100),@valorfirma varchar(max),@xmlgenerado varchar(max)) as
begin
update documento set nombrexml=@nombrexml, valorresumen= @valorresumen, valorfirma=@valorfirma, xmlgenerado=@xmlgenerado where empresa_id=@empresa_id and id=@documento_id and tipodocumento_id=@tipodocumento_id;
end


GO


-- ----------------------------
--  Procedure definition for `usp_buscar_tabla_codigosunat`
-- ----------------------------
  SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_buscar_tabla_codigosunat](@flag int, @codtabla varchar(50),@codbusqueda varchar(50)) as
begin
    IF @flag = 0
        SELECT codsunat FROM codigosunat where codtabla=@codtabla and codalterno = @codbusqueda;
    IF @flag=1 
        SELECT codsunat FROM codigosunat where codtabla=@codtabla and codelemento = @codbusqueda;
    IF @flag=2
        SELECT descripcion FROM codigosunat where codtabla=@codtabla and codsunat = @codbusqueda;
    IF @flag=3
        select descripcion from codigosunat where codtabla = @codtabla and codelemento = @codbusqueda;
    IF @flag=4
        select codalterno from codigosunat where codtabla = @codtabla and codelemento = @codbusqueda 
    
end

GO

-- ----------------------------
--  Procedure definition for `Usp_eliminar_documento`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Usp_eliminar_documento](@id VARCHAR(15), @empresa_id INT, @tipodocumento_id VARCHAR(2)) as
DECLARE @cantidad INT;
SELECT @cantidad= COUNT(*) FROM documento where id=@id and empresa_id=@empresa_id and tipodocumento_id=@tipodocumento_id and estado='N' and situacion in ('E','P');
if @cantidad = 1 
begin
delete from docrefeguia where documento_id=@id and empresa_id=@empresa_id and tipodocumento_id=@tipodocumento_id;
delete from docanticipo where documento_id=@id and empresa_id=@empresa_id and tipodocumento_id=@tipodocumento_id;
delete from docreferencia where documento_id=@id and empresa_id=@empresa_id and tipodocumento_id=@tipodocumento_id;
delete from detalledocumento where documento_id=@id and empresa_id=@empresa_id and tipodocumento_id=@tipodocumento_id;
delete from documento where id=@id and empresa_id=@empresa_id and tipodocumento_id=@tipodocumento_id;
end
else
RAISERROR (15600,-1,-1, 'eliminar_documento'); 





GO


-- ----------------------------
--  Procedure definition for `usp_insert_docanticipo`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_insert_docanticipo]
(@documento_id		varchar(15),
@empresa_id			int,
@tipodocumento_id	varchar(2),
@line_id			int,
@tipodocanticipo	varchar(2),
@nrodocanticipo		varchar(20),
@montoanticipo		float,
@tipodocemisor		varchar(3),
@nrodocemisor		varchar(15)
)
as
insert into docanticipo(documento_id,empresa_id,tipodocumento_id,line_id,tipodocanticipo,nrodocanticipo,montoanticipo,tipodocemisor,nrodocemisor)
values(@documento_id,@empresa_id,@tipodocumento_id,@line_id,@tipodocanticipo,@nrodocanticipo,@montoanticipo,@tipodocemisor,@nrodocemisor)

GO



-- ----------------------------
--  Procedure definition for `usp_insert_docrefeguia`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_insert_docrefeguia]
(@empresa_id	int,
@documento_id	varchar(15),
@tipodocumento_id	varchar(2),
@tipoguia	varchar(2),
@numeroguia	varchar(20)
)
as
insert into docrefeguia(empresa_id,documento_id,tipodocumento_id,tipo_guia,numero_guia)
values(@empresa_id,@documento_id,@tipodocumento_id,@tipoguia,@numeroguia)

GO





-- ----------------------------
--  Procedure definition for `Usp_listar_certificado`
-- ----------------------------


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_listar_certificado](@id int,@idEmpresa int, @nombre varchar(255)) as
begin

select id, empresa_id, nombre, ubicacion, username, clave, clavecertificado, idSignature, flagOSE
 from certificado where borrado=0
    and id= CASE WHEN @id !=0 THEN @id else id  END
    and  empresa_id = case when @idEmpresa !=0 then  @idEmpresa else empresa_id END
    and nombre like '%' + @nombre + '%'
    order by nombre asc;
end




GO





-- ----------------------------
--  Procedure definition for `Usp_listar_detalledocumento`
-- ----------------------------



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_listar_detalledocumento](
    @id varchar(255), 
    @Idempresa varchar(255), 
    @Idtipodocumento varchar(255)) as
begin
select d.empresa_id, 
dd.documento_id, 
d.tipodocumento_id, 
line_id, codigoproducto,
codigosunat,
nombreproducto,
afectoigv, 
afectoisc, tipoafectacionigv, tipocalculoisc, unidad, cantidad, 
preciounitario, precioreferencial, valorunitario, valorbruto, valordscto, valorcargo,  
valorventa, isc, igv, total, factorigv, factorisc, tbvt_puntoorigen, 
tbvt_descripcionorigen, tbvt_puntodestino, tbvt_descripciondestino, 
tbvt_detalleviaje, tbvt_valorrefpreliminar, 
tbvt_valorrefcargaefectiva, tbvt_valorrefcargautil 
from detalledocumento dd  
inner join documento d on d.id=dd.documento_id and d.empresa_id=dd.empresa_id and d.tipodocumento_id= dd.tipodocumento_id 
where 1=1
and dd.documento_id =       CASE WHEN @id !=''              THEN @id              else dd.documento_id  END
and dd.empresa_id =         CASE WHEN @Idempresa !='0'      THEN @Idempresa       else dd.empresa_id  END
and dd.tipodocumento_id =   CASE WHEN @Idtipodocumento !='' THEN @Idtipodocumento else dd.tipodocumento_id  END
order by line_id;
end



GO




-- ----------------------------
--  Procedure definition for `Usp_listar_docpersona`
-- ----------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_listar_docpersona](@codigo varchar(255),@estado varchar(255), @indpersona varchar(255)) as
begin

select codigo, codigosunat, nombre, longitud, tipo, indcontrib, indlongexacta, indpersona, orden, estado from docpersona where 1=1
 

    and codigo= CASE WHEN @codigo != '' THEN @codigo else codigo  END
    and  estado = case when @estado != '' then  @estado else estado END
    and  indpersona = case when @indpersona != '' then  @indpersona else indpersona END
    order by orden asc;
end






GO






-- ----------------------------
--  Procedure definition for `Usp_listar_docRefeGuia`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_listar_docRefeGuia](
    @id varchar(255), 
    @Idempresa varchar(255), 
    @Idtipodocumento varchar(255)) as
begin

select d.*, c.descripcion as desctipoguia from docrefeguia d 
left join codigosunat c on d.tipo_guia=c.codsunat and c.codtabla='001'
 where 1=1 
  and d.documento_id = CASE WHEN @id != '' THEN @id else d.documento_id  END
    and d.empresa_id = CASE WHEN @Idempresa != '0' THEN @Idempresa else d.empresa_id  END
    and d.tipodocumento_id = CASE WHEN @Idtipodocumento != '' THEN @Idtipodocumento else d.tipodocumento_id  END


end



GO





-- ----------------------------
--  Procedure definition for `Usp_listar_documento`
-- ----------------------------


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Usp_listar_documento](
    @id varchar(255),
    @Idempresa int,
    @Idtipodocumento varchar(255),
    @Estado varchar(255),
    @Situacion varchar(255),
    @Serie varchar(255),
    @Numero varchar(255),
    @FechaIni varchar(255) = null,
    @FechaFin varchar(255) = null,
    @opeafec varchar(255),
    @opeinafec varchar(255),
    @operetir varchar(255),
    @opexoner varchar(255),
    @opexport varchar(255)
    ) as
begin

select d.*,td.descripcion as tipodocumento,
e.nrodocumento empnrodoc, e.razonsocial empnombre, pv.descripcion puntoventadescripcion,
 pv.codigoestablec,
round(((select SUM(case when tipoafectacionigv in (@opeafec) then valorventa else 0 END) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) valopergravadas,  
round(((select SUM(case when tipoafectacionigv in (@opeinafec) then valorventa else 0 END) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) valoperinafectas, 
round(((select SUM(case when tipoafectacionigv in (@opexoner) then valorventa else 0 END) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) valoperexoneradas, 
round(((select SUM(case when (tipoafectacionigv in (@opexport) and valorventa > 0) then valorventa when (tipoafectacionigv in (@opexport) and valorventa = 0) then precioreferencial*cantidad else 0 END) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) valoperexportacion, 
round(((select SUM(case when tipoafectacionigv in (@operetir) then valorunitario*cantidad else 0 END) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) valopergratuitas, 
round(((select SUM(case when tipocalculoisc = '03' then (precioreferencial * cantidad) when tipocalculoisc = '02' then valorventa when tipocalculoisc = '01' then valorventa else 0 END) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) valoperiscreferenc, 
round(((select SUM(igv) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id and tipoafectacionigv in (@operetir))),2) sumaigvgratuito,
round(((select SUM(isc) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id and tipoafectacionigv in (@operetir))),2) sumaiscgratuito, 
round(((select SUM(valordscto) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) totaldsctoitem, 
round(((select SUM(valorcargo) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) totalcargoitem,  
(select descripcion from codigosunat where codtabla = '054' and codsunat = d.codigobbsssujetodetrac) descripciondetrac, 
(select descripcion from codigosunat where codtabla = '009' and codsunat = d.tiponotacreddeb) desctiponotcreddeb, 
(select descripcion from codigosunat where codtabla = '022' and codsunat = d.regimenpercep) descregimenpercep 
    from documento d inner join tipodocumento td on td.id=d.tipodocumento_id inner join empresa e on e.id = d.empresa_id left join puntoventa pv on e.id = pv.empresa_id and d.empresa_id = pv.empresa_id and d.puntoventa_id=pv.id where 1=1 
    and d.id = CASE WHEN @id != '' THEN @id else d.id  END
    and d.empresa_id = CASE WHEN @Idempresa != '0' THEN @Idempresa else d.empresa_id  END
    and d.tipodocumento_id = CASE WHEN @Idtipodocumento != '' THEN @Idtipodocumento else d.tipodocumento_id  END
    and d.estado= CASE WHEN @Estado != '' THEN @Estado else d.estado  END
    and d.situacion= CASE WHEN @Situacion != '' THEN @Situacion else d.situacion  END
    and d.serie_comprobante like CASE WHEN @Serie != '' THEN @Serie+'%' else '%'+d.serie_comprobante+'%'  END
    and d.nro_comprobante like CASE WHEN @Numero != '' THEN '%'+@Numero+'%' else '%'+d.nro_comprobante+'%'  END
    and d.fecha BETWEEN coalesce(@FechaIni,d.fecha) and coalesce(@FechaFin,d.fecha)
    
order by d.fecha desc, d.serie_comprobante, d.nro_comprobante asc;
end





GO


-- ----------------------------
--  Procedure definition for `Usp_listar_documentoanticipo`
-- ----------------------------



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_listar_documentoanticipo](
    @id varchar(255), 
    @Idempresa varchar(255), 
    @Idtipodocumento varchar(255)) as
begin
select d.*, c.descripcion as desctipodocant, t.codelemento as desctipodocemi
 from docanticipo d left join codigosunat c on d.tipodocanticipo=c.codsunat and c.codtabla='012' 
 left join codigosunat t on d.tipodocemisor=t.codsunat and t.codtabla='006'
 where 1=1  
and d.documento_id =       CASE WHEN @id !=''              THEN @id              else d.documento_id  END
and d.empresa_id =         CASE WHEN @Idempresa !='0'      THEN @Idempresa       else d.empresa_id  END
and d.tipodocumento_id =   CASE WHEN @Idtipodocumento !='' THEN @Idtipodocumento else d.tipodocumento_id  END

end



GO



-- ----------------------------
--  Procedure definition for `Usp_listar_documentoguia`
-- ----------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_listar_documentoguia](
    @id varchar(255), 
    @Idempresa varchar(255), 
    @Idtipodocumento varchar(255)) as
begin

select d.documento_id ,d.empresa_id ,d.tipodocumento_id ,d.partida_direccion ,d.partida_urbanizacion ,d.partida_departamento ,
d.partida_provincia ,d.partida_distrito ,d.partida_pais ,(select descripcion from udepartamentos where id=d.partida_departamento) descpartida_departamento,
(select descripcion from uprovincias where id=d.partida_provincia) descpartida_provincia, 
(select descripcion from udistritos where id=d.partida_distrito) descpartida_distrito,
d.llegada_direccion ,d.llegada_urbanizacion ,d.llegada_departamento ,d.llegada_provincia ,d.llegada_distrito , 
(select descripcion from udepartamentos where id=d.llegada_departamento) descllegada_departamento, 
(select descripcion from uprovincias where id=d.llegada_provincia) descllegada_provincia, 
(select descripcion from udistritos where id=d.llegada_distrito) descllegada_distrito,
d.llegada_pais ,d.vehiculo_placa ,d.vehiculo_constancia ,d.vehiculo_marca ,d.licencia_conducir ,d.transportista_ruc ,
d.transportista_razonsocial ,d.modalidad_transporte ,(case when d.modalidad_transporte = '01' then 'Transporte Público' else 'Transporte Privado' end) desc_modalidad_transporte, 
d.peso_unidad ,d.peso_cantidad  from facturaguia d where 1 = 1  
and d.documento_id = @id
and d.empresa_id = @Idempresa
and d.tipodocumento_id = @Idtipodocumento;


end



GO





-- ----------------------------
--  Procedure definition for `Usp_listar_documentoreferencia`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_listar_documentoreferencia](
    @id varchar(255), 
    @Idempresa varchar(255), 
    @Idtipodocumento varchar(255)) as
begin
select d.*, t.descripcion 
from docreferencia d inner join tipodocumento t on d.tipodocumentoref_id=t.id 
where 1=1  
and d.documento_id =       CASE WHEN @id !=''              THEN @id              else d.documento_id  END
and d.empresa_id =         CASE WHEN @Idempresa !='0'      THEN @Idempresa       else d.empresa_id  END
and d.tipodocumento_id =   CASE WHEN @Idtipodocumento !='' THEN @Idtipodocumento else d.tipodocumento_id  END

end



GO




-- ----------------------------
--  Procedure definition for `Usp_listar_empresa`
-- ----------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_listar_empresa](
    @IdEmpresa int, 
    @Nrodocumento varchar(255), 
    @RazonSocial  varchar(255),
    @Estado  varchar(255),
    @Idpuntoventa int) as
begin

select e.*,pv.codpais codpais, pv.coddis coddis,
 d.descripcion departamento, p.descripcion provincia, t.descripcion distrito  from empresa e 
left join (select * from puntoventa pv0 where pv0.id= @Idpuntoventa and pv0.empresa_id=@IdEmpresa) pv on pv.empresa_id=e.id
left join udistritos t on pv.coddis=t.id  
left join uprovincias p on t.idprov=p.id 
left join udepartamentos d on p.iddep=d.id 


where e.borrado=0 
    and e.id = CASE WHEN @IdEmpresa != 0 THEN @IdEmpresa else e.id  END
    and e.nrodocumento like   CASE WHEN @Nrodocumento != '' THEN '%'+@Nrodocumento+'%' else '%'+e.nrodocumento+'%'   END
    and e.razonsocial like CASE WHEN @RazonSocial != '' THEN '%'+@RazonSocial+'%' else '%'+e.razonsocial+'%' END
    and e.estado  = CASE WHEN @Estado != '' THEN  @Estado else e.estado  END

    order by e.razonsocial asc;

end







GO




-- ----------------------------
--  Procedure definition for `Usp_registrar_deta_documento`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Usp_registrar_deta_documento] (@documento_id      VARCHAR(15),
                                               @empresa_id					INT,
                                               @tipodocumento_id			VARCHAR(2),
                                               @line_id						int,
                                               @codigoproducto				VARCHAR(50),
                                               @codigosunat					VARCHAR(50),
                                               @nombreproducto				VARCHAR(200),
                                               @afectoigv					CHAR(2),
                                               @afectoisc					CHAR(2),
                                               @tipoafectacionigv			CHAR(2),
                                               @tipocalculoisc				CHAR(2),
                                               @unidad						VARCHAR(50),
                                               @cantidad					FLOAT,
                                               @preciounitario				FLOAT,
                                               @precioreferencial			FLOAT,
                                               @valorunitario				FLOAT,
                                               @valorbruto					FLOAT,
                                               @valordscto					FLOAT,
                                               @valorcargo					FLOAT,
                                               @valorventa					FLOAT,
                                               @isc							FLOAT,
                                               @igv							FLOAT,
                                               @total						FLOAT,
                                               @factorigv					FLOAT,
                                               @factorisc					FLOAT,
											   @tbvt_puntoorigen			VARCHAR(100),
											   @tbvt_descripcionorigen		VARCHAR(100),
											   @tbvt_puntodestino			VARCHAR(100),
											   @tbvt_descripciondestino		VARCHAR(100),
											   @tbvt_detalleviaje			VARCHAR(100),
											   @tbvt_valorrefpreliminar		VARCHAR(100),
											   @tbvt_valorrefcargaefectiva	VARCHAR(100),
											   @tbvt_valorrefcargautil		VARCHAR(100)											   
											   )
AS
    INSERT INTO detalledocumento
                (documento_id,
                 empresa_id,
                 tipodocumento_id,
                 line_id,
                 codigoproducto,
				 codigosunat,
                 nombreproducto,
                 afectoigv,
                 afectoisc,
                 tipoafectacionigv,
                 tipocalculoisc,
                 unidad,
                 cantidad,
                 preciounitario,
				 precioreferencial,
                 valorunitario,
                 valorbruto,
                 valordscto,
                 valorcargo,
                 valorventa,
                 isc,
                 igv,
                 total,
				 factorigv,
                 factorisc,
                 tbvt_puntoorigen,
				 tbvt_descripcionorigen,
				 tbvt_puntodestino,
				 tbvt_descripciondestino,
				 tbvt_detalleviaje,
				 tbvt_valorrefpreliminar,
				 tbvt_valorrefcargaefectiva,
				 tbvt_valorrefcargautil
				 )
    VALUES      ( @documento_id,
                  @empresa_id,
                  @tipodocumento_id,
                  @line_id,
                  @codigoproducto,
				  @codigosunat,
                  @nombreproducto,
                  @afectoigv,
                  @afectoisc,
                  @tipoafectacionigv,
                  @tipocalculoisc,
                  @unidad,
                  @cantidad,
                  @preciounitario,
				  @precioreferencial,
                  @valorunitario,
                  @valorbruto,
                  @valordscto,
				  @valorcargo,
                  @valorventa,
                  @isc,
                  @igv,
                  @total,
                  @factorigv,
                  @factorisc,
				  @tbvt_puntoorigen,
				  @tbvt_descripcionorigen,
				  @tbvt_puntodestino,
				  @tbvt_descripciondestino,
				  @tbvt_detalleviaje,
				  @tbvt_valorrefpreliminar,
				  @tbvt_valorrefcargaefectiva,
				  @tbvt_valorrefcargautil
				  ) 


GO

-- ----------------------------
--  Procedure definition for `Usp_registrar_documento`
-- ----------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Usp_registrar_documento] (@id							VARCHAR(15),
												@empresa_id						INT,
												@tipodocumento_id				VARCHAR(2),
												@serie_comprobante				VARCHAR(5),
												@nro_comprobante				VARCHAR(10),
												@puntoventa_id					INT,
												@tipooperacion					VARCHAR(4),
												@tiponotacreddeb				VARCHAR(2),
												@cliente_tipodoc				VARCHAR(3),
												@cliente_nrodoc					VARCHAR(20),
												@cliente_nombre					VARCHAR(200),
												@cliente_direccion				VARCHAR(200),
												@cliente_email					VARCHAR(200),
												@fecha							DATE,
												@hora							VARCHAR(8),
												@fechavencimientopago			DATE,
												@glosa							VARCHAR(200),
												@moneda							VARCHAR(5),
												@tipocambio						FLOAT,
												@descuento						FLOAT,
												@igvventa						FLOAT,
												@iscventa						FLOAT,
												@otrostributos					FLOAT,
												@otroscargos					FLOAT,
												@importeventa					FLOAT,
												@regimenpercep					VARCHAR(2),
												@porcentpercep					FLOAT,
												@importepercep					FLOAT,
												@porcentdetrac					FLOAT,
												@importedetrac					FLOAT,
												@importerefdetrac				FLOAT,
												@importefinal					FLOAT,
												@importeanticipo				FLOAT,
												@indicaanticipo					CHAR(1),
												@estado							CHAR(1),
												@situacion						CHAR(1),
												@numeroelectronico				VARCHAR(50),
												@estransgratuita				CHAR(2),
												@enviado_email					CHAR(2),
												@enviado_externo				CHAR(2),
												@tipoguiaremision				CHAR(2),
												@guiaremision					VARCHAR(30),
												@tipodocotrodocref				CHAR(2),
												@otrodocumentoref				VARCHAR(30),
												@ordencompra					VARCHAR(20),
												@placavehiculo					VARCHAR(8),
												@montofise						DECIMAL(8,2),
												@tiporegimen					varchar(3),
												@codigobbsssujetodetrac			varchar(20),
												@numctabconacion				varchar(30),
												@bientransfamazonia				char(1),
												@servitransfamazonia			char(1),
												@contratoconstamazonia			char(1),
                                                @formapago			            varchar(255),
                                                @jsonpagocredito			    varchar(max),
                                                @hasRetencionIgv                char(2),
                                                @porcRetIgv                     float = null,
                                                @impRetIgv                      DECIMAL(12,2) = null,
                                                @impOpeRetIgv                   DECIMAL(8,2) = null
                                                )


AS
    INSERT INTO documento
                (id,
                 empresa_id,
                 tipodocumento_id,
                 serie_comprobante,
                 nro_comprobante,
                 puntoventa_id,
				 tipooperacion,
                 tiponotacreddeb,
                 cliente_tipodoc,
                 cliente_nrodoc,
                 cliente_nombre,
                 cliente_direccion,
                 cliente_email,
                 fecha,
				 hora,
                 fechavencimientopago,
                 glosa,
                 moneda,
                 tipocambio,
                 descuento,
                 igvventa,
                 iscventa,
                 otrostributos,
                 otroscargos,
                 importeventa,
                 regimenpercep,
                 porcentpercep,
                 importepercep,
                 porcentdetrac,
                 importedetrac,
				 importerefdetrac,
                 importefinal,
				 importeanticipo,
				 indicaanticipo,
                 estado,
                 situacion,
                 numeroelectronico,
                 estransgratuita,
                 enviado_email,
                 enviado_externo,
				 tipoguiaremision,
				 guiaremision,
				 tipodocotrodocref,
				 otrodocumentoref,
				 ordencompra,
				 placavehiculo,
				 montofise,
				 tiporegimen,
				 codigobbsssujetodetrac,
				 numctabconacion,
				 bientransfamazonia,
				 servitransfamazonia,
				 contratoconstamazonia,
				 fecreg,
                 formapago,
                 jsonpagocredito,
                 hasRetencionIgv,
                 porcRetIgv,
                 impRetIgv,
                 impOpeRetIgv)
    VALUES      (@id,
                 @empresa_id,
                 @tipodocumento_id,
                 @serie_comprobante,
                 @nro_comprobante,
                 @puntoventa_id,
				 @tipooperacion,
                 @tiponotacreddeb,
                 @cliente_tipodoc,
                 @cliente_nrodoc,
                 @cliente_nombre,
                 @cliente_direccion,
                 @cliente_email,
                 @fecha,
				 @hora,
                 @fechavencimientopago,
                 @glosa,
                 @moneda,
                 @tipocambio,
                 @descuento,
                 @igvventa,
                 @iscventa,
                 @otrostributos,
                 @otroscargos,
                 @importeventa,
                 @regimenpercep,
                 @porcentpercep,
                 @importepercep,
                 @porcentdetrac,
                 @importedetrac,
				 @importerefdetrac,
                 @importefinal,
				 @importeanticipo,
				 @indicaanticipo,
                 @estado,
                 @situacion,
                 @numeroelectronico,
                 @estransgratuita,
                 @enviado_email,
                 @enviado_externo,
				 @tipoguiaremision,
				 @guiaremision,
				 @tipodocotrodocref,
				 @otrodocumentoref,
				 @ordencompra,
				 @placavehiculo,
				 @montofise,
				 @tiporegimen,
				 @codigobbsssujetodetrac,
				 @numctabconacion,
				 @bientransfamazonia,
				 @servitransfamazonia,
				 @contratoconstamazonia,
				 GETDATE(),
                 @formapago,
                 @jsonpagocredito,
                 @hasRetencionIgv,
                 @porcRetIgv,
                 @impRetIgv,
                 @impOpeRetIgv)





GO



-- ----------------------------
--  Procedure definition for `Usp_validar_empresacertificado`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_validar_empresacertificado](@id_empresa int,@usuario varchar(255), @clave varchar(255)) as
begin

select nombre from certificado where borrado=0 and empresa_id=@id_empresa and username=@usuario and clave=@clave; 
end





GO



