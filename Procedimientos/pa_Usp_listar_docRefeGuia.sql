SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Juan Alarcon
-- Create date: <Create Date,27/03/2021>
-- Description:	<Description>
-- =============================================
CREATE PROCEDURE [dbo].[Usp_listar_docRefeGuia](
    @id varchar(255), 
    @Idempresa varchar(255), 
    @Idtipodocumento varchar(255)) as
begin

select d.*, c.descripcion as desctipoguia from docrefeguia d 
left join codigosunat c on d.tipo_guia=c.codsunat and c.codtabla='001'
 where 1=1 
  and d.documento_id = CASE WHEN @id != '' THEN @id else d.documento_id  END
    and d.empresa_id = CASE WHEN @Idempresa != '0' THEN @Idempresa else d.empresa_id  END
    and d.tipodocumento_id = CASE WHEN @Idtipodocumento != '' THEN @Idtipodocumento else d.tipodocumento_id  END


end



GO
