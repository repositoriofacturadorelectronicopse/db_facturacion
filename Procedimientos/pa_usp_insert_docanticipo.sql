SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Juan Alarcon>
-- Create date: <Create Date,28/03/2021>
-- Description:	<Description>
-- =============================================
CREATE procedure [dbo].[usp_insert_docanticipo]
(@documento_id		varchar(15),
@empresa_id			int,
@tipodocumento_id	varchar(2),
@line_id			int,
@tipodocanticipo	varchar(2),
@nrodocanticipo		varchar(20),
@montoanticipo		float,
@tipodocemisor		varchar(3),
@nrodocemisor		varchar(15)
)
as
insert into docanticipo(documento_id,empresa_id,tipodocumento_id,line_id,tipodocanticipo,nrodocanticipo,montoanticipo,tipodocemisor,nrodocemisor)
values(@documento_id,@empresa_id,@tipodocumento_id,@line_id,@tipodocanticipo,@nrodocanticipo,@montoanticipo,@tipodocemisor,@nrodocemisor)

GO
