SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Juan Alarcon>
-- Create date: <Create Date,27/03/2021>
-- Description:	<Description>
-- =============================================
CREATE PROCEDURE [dbo].[Usp_listar_detalledocumento](
    @id varchar(255), 
    @Idempresa varchar(255), 
    @Idtipodocumento varchar(255)) as
begin
select d.empresa_id, 
dd.documento_id, 
d.tipodocumento_id, 
line_id, codigoproducto,
codigosunat,
nombreproducto,
afectoigv, 
afectoisc, tipoafectacionigv, tipocalculoisc, unidad, cantidad, 
preciounitario, precioreferencial, valorunitario, valorbruto, valordscto, valorcargo,  
valorventa, isc, igv, total, factorigv, factorisc, tbvt_puntoorigen, 
tbvt_descripcionorigen, tbvt_puntodestino, tbvt_descripciondestino, 
tbvt_detalleviaje, tbvt_valorrefpreliminar, 
tbvt_valorrefcargaefectiva, tbvt_valorrefcargautil 
from detalledocumento dd  
inner join documento d on d.id=dd.documento_id and d.empresa_id=dd.empresa_id and d.tipodocumento_id= dd.tipodocumento_id 
where 1=1
and dd.documento_id =       CASE WHEN @id !=''              THEN @id              else dd.documento_id  END
and dd.empresa_id =         CASE WHEN @Idempresa !='0'      THEN @Idempresa       else dd.empresa_id  END
and dd.tipodocumento_id =   CASE WHEN @Idtipodocumento !='' THEN @Idtipodocumento else dd.tipodocumento_id  END
order by line_id;
end



GO
