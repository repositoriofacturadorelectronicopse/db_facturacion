SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Juan Alarcon
-- Create date: <Create Date,27/03/2021>
-- Description:	<Description>
-- =============================================
CREATE PROCEDURE [dbo].[Usp_listar_documentoguia](
    @id varchar(255), 
    @Idempresa varchar(255), 
    @Idtipodocumento varchar(255)) as
begin

select d.documento_id ,d.empresa_id ,d.tipodocumento_id ,d.partida_direccion ,d.partida_urbanizacion ,d.partida_departamento ,
d.partida_provincia ,d.partida_distrito ,d.partida_pais ,(select descripcion from udepartamentos where id=d.partida_departamento) descpartida_departamento,
(select descripcion from uprovincias where id=d.partida_provincia) descpartida_provincia, 
(select descripcion from udistritos where id=d.partida_distrito) descpartida_distrito,
d.llegada_direccion ,d.llegada_urbanizacion ,d.llegada_departamento ,d.llegada_provincia ,d.llegada_distrito , 
(select descripcion from udepartamentos where id=d.llegada_departamento) descllegada_departamento, 
(select descripcion from uprovincias where id=d.llegada_provincia) descllegada_provincia, 
(select descripcion from udistritos where id=d.llegada_distrito) descllegada_distrito,
d.llegada_pais ,d.vehiculo_placa ,d.vehiculo_constancia ,d.vehiculo_marca ,d.licencia_conducir ,d.transportista_ruc ,
d.transportista_razonsocial ,d.modalidad_transporte ,(case when d.modalidad_transporte = '01' then 'Transporte Público' else 'Transporte Privado' end) desc_modalidad_transporte, 
d.peso_unidad ,d.peso_cantidad  from facturaguia d where 1 = 1  
and d.documento_id = @id
and d.empresa_id = @Idempresa
and d.tipodocumento_id = @Idtipodocumento;


end



GO
