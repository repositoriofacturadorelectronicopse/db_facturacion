SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Juan Alarcon>
-- Create date: <Create Date,27/03/2021>
-- Description:	<Description>
-- =============================================
CREATE PROCEDURE [dbo].[actualizar_facturaxml](@empresa_id int,@documento_id varchar(15),@tipodocumento_id varchar(2), @nombrexml varchar(100), @valorresumen varchar(100),@valorfirma varchar(max),@xmlgenerado varchar(max)) as
begin
update documento set nombrexml=@nombrexml, valorresumen= @valorresumen, valorfirma=@valorfirma, xmlgenerado=@xmlgenerado where empresa_id=@empresa_id and id=@documento_id and tipodocumento_id=@tipodocumento_id;
end


GO
