SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Juan Alarcon>
-- Create date: <Create Date,28/03/2021>
-- Description:	<Description>
-- =============================================
create procedure [dbo].[usp_insert_docreferencia]
(@empresa_id	int,
@documento_id	varchar(15),
@tipodocumento_id	varchar(2),
@documentoref_id	varchar(15),
@tipodocumentoref_id	varchar(2),
@numerodocreferencia	varchar(20)
)
as
insert into docreferencia(empresa_id,documento_id,tipodocumento_id,documentoref_id,tipodocumentoref_id,numerodocreferencia)
values(@empresa_id,@documento_id,@tipodocumento_id,@documentoref_id,@tipodocumentoref_id,@numerodocreferencia)

GO
