SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Juan Alarcon>
-- Create date: <Create Date,27/03/2021>
-- Description:	<Description>
-- =============================================
CREATE PROCEDURE [dbo].[Usp_listar_documento](
    @id varchar(255),
    @Idempresa int,
    @Idtipodocumento varchar(255),
    @Estado varchar(255),
    @Situacion varchar(255),
    @Serie varchar(255),
    @Numero varchar(255),
    @FechaIni varchar(255) = null,
    @FechaFin varchar(255) = null,
    @opeafec varchar(255),
    @opeinafec varchar(255),
    @operetir varchar(255),
    @opexoner varchar(255),
    @opexport varchar(255)
    ) as
begin

select d.*,td.descripcion as tipodocumento,
e.nrodocumento empnrodoc, e.razonsocial empnombre, pv.descripcion puntoventadescripcion,
 pv.codigoestablec,
round(((select SUM(case when tipoafectacionigv in (@opeafec) then valorventa else 0 END) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) valopergravadas,  
round(((select SUM(case when tipoafectacionigv in (@opeinafec) then valorventa else 0 END) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) valoperinafectas, 
round(((select SUM(case when tipoafectacionigv in (@opexoner) then valorventa else 0 END) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) valoperexoneradas, 
round(((select SUM(case when (tipoafectacionigv in (@opexport) and valorventa > 0) then valorventa when (tipoafectacionigv in (@opexport) and valorventa = 0) then precioreferencial*cantidad else 0 END) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) valoperexportacion, 
round(((select SUM(case when tipoafectacionigv in (@operetir) then valorunitario*cantidad else 0 END) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) valopergratuitas, 
round(((select SUM(case when tipocalculoisc = '03' then (precioreferencial * cantidad) when tipocalculoisc = '02' then valorventa when tipocalculoisc = '01' then valorventa else 0 END) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) valoperiscreferenc, 
round(((select SUM(igv) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id and tipoafectacionigv in (@operetir))),2) sumaigvgratuito,
round(((select SUM(isc) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id and tipoafectacionigv in (@operetir))),2) sumaiscgratuito, 
round(((select SUM(valordscto) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) totaldsctoitem, 
round(((select SUM(valorcargo) from detalledocumento where documento_id=d.id and empresa_id=d.empresa_id and tipodocumento_id=d.tipodocumento_id)),2) totalcargoitem,  
(select descripcion from codigosunat where codtabla = '054' and codsunat = d.codigobbsssujetodetrac) descripciondetrac, 
(select descripcion from codigosunat where codtabla = '009' and codsunat = d.tiponotacreddeb) desctiponotcreddeb, 
(select descripcion from codigosunat where codtabla = '022' and codsunat = d.regimenpercep) descregimenpercep 
    from documento d inner join tipodocumento td on td.id=d.tipodocumento_id inner join empresa e on e.id = d.empresa_id left join puntoventa pv on e.id = pv.empresa_id and d.empresa_id = pv.empresa_id and d.puntoventa_id=pv.id where 1=1 
    and d.id = CASE WHEN @id != '' THEN @id else d.id  END
    and d.empresa_id = CASE WHEN @Idempresa != '0' THEN @Idempresa else d.empresa_id  END
    and d.tipodocumento_id = CASE WHEN @Idtipodocumento != '' THEN @Idtipodocumento else d.tipodocumento_id  END
    and d.estado= CASE WHEN @Estado != '' THEN @Estado else d.estado  END
    and d.situacion= CASE WHEN @Situacion != '' THEN @Situacion else d.situacion  END
    and d.serie_comprobante like CASE WHEN @Serie != '' THEN @Serie+'%' else '%'+d.serie_comprobante+'%'  END
    and d.nro_comprobante like CASE WHEN @Numero != '' THEN '%'+@Numero+'%' else '%'+d.nro_comprobante+'%'  END
    and d.fecha BETWEEN coalesce(@FechaIni,d.fecha) and coalesce(@FechaFin,d.fecha)
    
order by d.fecha desc, d.serie_comprobante, d.nro_comprobante asc;
end





GO
