SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Juan Alarcon>
-- Create date: <Create Date,28/03/2021>
-- Description:	<Description>
-- =============================================
CREATE PROCEDURE [dbo].[usp_buscar_tabla_codigosunat](@flag int, @codtabla varchar(50),@codbusqueda varchar(50)) as
begin
    IF @flag = 0
        SELECT codsunat FROM codigosunat where codtabla=@codtabla and codalterno = @codbusqueda;
    IF @flag=1 
        SELECT codsunat FROM codigosunat where codtabla=@codtabla and codelemento = @codbusqueda;
    IF @flag=2
        SELECT descripcion FROM codigosunat where codtabla=@codtabla and codsunat = @codbusqueda;
    IF @flag=3
        select descripcion from codigosunat where codtabla = @codtabla and codelemento = @codbusqueda;
    IF @flag=4
        select codalterno from codigosunat where codtabla = @codtabla and codelemento = @codbusqueda 
    
end







GO
