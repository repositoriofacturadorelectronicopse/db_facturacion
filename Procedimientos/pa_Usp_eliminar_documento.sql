SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Juan Alarcon>
-- Create date: <Create Date,28/03/2021>
-- Description:	<Description>
-- =============================================
CREATE procedure [dbo].[Usp_eliminar_documento](@id VARCHAR(15), @empresa_id INT, @tipodocumento_id VARCHAR(2)) as
DECLARE @cantidad INT;
SELECT @cantidad= COUNT(*) FROM documento where id=@id and empresa_id=@empresa_id and tipodocumento_id=@tipodocumento_id and estado='N' and situacion in ('E','P');
if @cantidad = 1 
begin
delete from docrefeguia where documento_id=@id and empresa_id=@empresa_id and tipodocumento_id=@tipodocumento_id;
delete from docanticipo where documento_id=@id and empresa_id=@empresa_id and tipodocumento_id=@tipodocumento_id;
delete from docreferencia where documento_id=@id and empresa_id=@empresa_id and tipodocumento_id=@tipodocumento_id;
delete from detalledocumento where documento_id=@id and empresa_id=@empresa_id and tipodocumento_id=@tipodocumento_id;
delete from documento where id=@id and empresa_id=@empresa_id and tipodocumento_id=@tipodocumento_id;
end
else
RAISERROR (15600,-1,-1, 'eliminar_documento'); 





GO
