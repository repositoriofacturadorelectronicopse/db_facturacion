
-- ----------------------------
--  Table structure for `docpersona`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[docpersona](
	[codigo] [char](3) NOT NULL,
	[codigosunat] [varchar](2) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[longitud] [int] NOT NULL,
	[tipo] [char](1) NOT NULL,
	[indcontrib] [char](1) NOT NULL,
	[indlongexacta] [char](1) NOT NULL,
	[indpersona] [char](1) NOT NULL,
	[orden] [int] NOT NULL,
	[estado] [char](1) NOT NULL,
	[usureg] [varchar](10) NOT NULL,
	[fecreg] [datetime] NOT NULL,
	[usumod] [varchar](10) NULL,
	[fecmod] [nchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[codigosunat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero de caracteres' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'docpersona', @level2type=N'COLUMN',@level2name=N'longitud'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0-Alfanumerico 1-Numerico' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'docpersona', @level2type=N'COLUMN',@level2name=N'tipo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0-Docum Solo Nacional 1-Docum Solo Extranjero 2-Doc Ambos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'docpersona', @level2type=N'COLUMN',@level2name=N'indcontrib'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0-LongMax 1-LongExact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'docpersona', @level2type=N'COLUMN',@level2name=N'indlongexacta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'N--Natural || J--Juridica || A--Ambos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'docpersona', @level2type=N'COLUMN',@level2name=N'indpersona'
GO


-- ----------------------------
--  Table structure for `empresa`
-- ----------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[empresa](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tipoempresa] [char](2) NOT NULL,
	[nrodocumento] [varchar](11) NOT NULL,
	[razonsocial] [varchar](200) NOT NULL,
	[nomcomercial] [varchar](100) NULL,
	[propietario] [varchar](200) NULL,
	[tipodocumento] [char](3) NOT NULL,
	[nrodoc] [varchar](20) NOT NULL,
	[apellido] [varchar](100) NULL,
	[nombre] [varchar](100) NULL,
	[direccion] [varchar](200) NOT NULL,
	[sector] [varchar](200) NULL,
	[telefono1] [varchar](20) NULL,
	[telefono2] [varchar](20) NULL,
	[email] [varchar](100) NULL,
	[web] [varchar](100) NULL,
	[imagen] [varchar](100) NULL,
	[signatureID] [varchar](20) NOT NULL,
	[signatureURI] [varchar](20) NOT NULL,
	[numeroresolucion] [varchar](200) NOT NULL,
	[cuentabcodet] [varchar](50) NULL,
	[aplicaisc] [char](1) NULL,
	[aplicadctoitem] [char](1) NULL,
	[aplicacargoitem] [char](1) NULL,
	[muestrasubtotal] [char](1) NULL,
	[muestraigv] [char](1) NULL,
	[aplicadctoglobal] [char](1) NULL,
	[aplicacargoglobal] [char](1) NULL,
	[estado] [char](3) NULL,
	[usureg] [varchar](10) NOT NULL,
	[fecreg] [datetime] NOT NULL,
	[usumod] [varchar](10) NULL,
	[fecmod] [datetime] NULL,
	[borrado] [char](1) NULL,
 CONSTRAINT [PK_empresa] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parametro para generacion XML' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'empresa', @level2type=N'COLUMN',@level2name=N'signatureID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parametro para generacion XML' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'empresa', @level2type=N'COLUMN',@level2name=N'signatureURI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cuenta del Bco de la Nacion donde aplica Detraccion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'empresa', @level2type=N'COLUMN',@level2name=N'cuentabcodet'
GO
-- ----------------------------
--  Table structure for `puntoventa`
-- ----------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[puntoventa](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[empresa_id] [int] NOT NULL,
	[descripcion] [varchar](200) NOT NULL,
	[codigoestablec] [varchar](10) NULL,
	[impresora] [varchar](150) NULL,
	[codpais] [varchar](2) NULL,
	[coddis] [varchar](6) NOT NULL,
	[estado] [char](1) NULL,
	[usureg] [varchar](10) NOT NULL,
	[fecreg] [datetime] NOT NULL,
	[usumod] [varchar](10) NULL,
	[fecmod] [datetime] NULL,
	[borrado] [char](1) NULL,
 CONSTRAINT [PK_puntoventa] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[puntoventa]  WITH CHECK ADD  CONSTRAINT [FK_puntoventa_coddis] FOREIGN KEY([coddis])
REFERENCES [dbo].[udistritos] ([id])
GO
ALTER TABLE [dbo].[puntoventa] CHECK CONSTRAINT [FK_puntoventa_coddis]
GO
ALTER TABLE [dbo].[puntoventa]  WITH CHECK ADD  CONSTRAINT [FK_puntoventa_codpais] FOREIGN KEY([codpais])
REFERENCES [dbo].[upais] ([id])
GO
ALTER TABLE [dbo].[puntoventa] CHECK CONSTRAINT [FK_puntoventa_codpais]
GO
ALTER TABLE [dbo].[puntoventa]  WITH CHECK ADD  CONSTRAINT [FK_puntoventa_empresa] FOREIGN KEY([empresa_id])
REFERENCES [dbo].[empresa] ([id])
GO
ALTER TABLE [dbo].[puntoventa] CHECK CONSTRAINT [FK_puntoventa_empresa]
GO


-- ----------------------------
--  Table structure for `certificado`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[certificado](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[empresa_id] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[ubicacion] [varchar](200) NOT NULL,
	[username] [varchar](50) NOT NULL,
	[clave] [varchar](50) NOT NULL,
	[clavecertificado] [varchar](50) NOT NULL,
	[idSignature] [varchar](50) NOT NULL,
	[flagOSE] [char](1) NULL,
	[borrado] [char](1) NOT NULL,
 CONSTRAINT [PK_certificado] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[certificado]  WITH CHECK ADD  CONSTRAINT [FK_certificado_empresa] FOREIGN KEY([empresa_id])
REFERENCES [dbo].[empresa] ([id])
GO
ALTER TABLE [dbo].[certificado] CHECK CONSTRAINT [FK_certificado_empresa]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id correlativo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'certificado', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id de empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'certificado', @level2type=N'COLUMN',@level2name=N'empresa_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre del certificado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'certificado', @level2type=N'COLUMN',@level2name=N'nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'directorio donde se ubica el certificado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'certificado', @level2type=N'COLUMN',@level2name=N'ubicacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario Secundario SUNAT / Usuario OSE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'certificado', @level2type=N'COLUMN',@level2name=N'username'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Clave de Usuario Secundario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'certificado', @level2type=N'COLUMN',@level2name=N'clave'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Clave del certificado digital' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'certificado', @level2type=N'COLUMN',@level2name=N'clavecertificado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor por default del certificado en la firma' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'certificado', @level2type=N'COLUMN',@level2name=N'idSignature'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 Indica SUNAT - 1 Indica OSE NubeFact - 2 Indica OSE EFact ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'certificado', @level2type=N'COLUMN',@level2name=N'flagOSE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 / 1 -- Indica si fue eliminado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'certificado', @level2type=N'COLUMN',@level2name=N'borrado'
GO

-- ----------------------------
--  Table structure for `codigosunat`
-- ----------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[codigosunat](
	[codtabla] [varchar](50) NOT NULL,
	[codelemento] [varchar](50) NOT NULL,
	[descripcion] [varchar](200) NOT NULL,
	[codsunat] [varchar](50) NOT NULL,
	[codalterno] [varchar](50) NOT NULL
) ON [PRIMARY]
GO


-- ----------------------------
--  Table structure for `documento`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[documento](
	[id] [varchar](15) NOT NULL,
	[empresa_id] [int] NOT NULL,
	[tipodocumento_id] [varchar](2) NOT NULL,
	[serie_comprobante] [varchar](5) NOT NULL,
	[nro_comprobante] [varchar](10) NOT NULL,
	[puntoventa_id] [int] NULL,
	[tipooperacion] [varchar](4) NULL,
	[tiponotacreddeb] [varchar](2) NULL,
	[cliente_tipodoc] [varchar](2) NOT NULL,
	[cliente_nrodoc] [varchar](20) NOT NULL,
	[cliente_nombre] [varchar](200) NOT NULL,
	[cliente_direccion] [varchar](200) NULL,
	[cliente_email] [varchar](200) NULL,
	[fecha] [date] NOT NULL,
	[hora] [varchar](8) NULL,
	[fechavencimientopago] [date] NULL,
	[glosa] [varchar](200) NULL,
	[moneda] [varchar](5) NOT NULL,
	[tipocambio] [float] NULL,
	[descuento] [float] NULL,
	[igvventa] [float] NULL,
	[iscventa] [float] NULL,
	[otrostributos] [float] NULL,
	[otroscargos] [float] NULL,
	[importeventa] [float] NOT NULL,
	[regimenpercep] [varchar](2) NULL,
	[porcentpercep] [float] NULL,
	[importepercep] [float] NULL,
	[porcentdetrac] [float] NULL,
	[importedetrac] [float] NULL,
	[importerefdetrac] [float] NULL,
	[importefinal] [float] NOT NULL,
	[importeanticipo] [float] NULL,
	[indicaanticipo] [varchar](1) NULL,
	[estado] [char](1) NOT NULL,
	[situacion] [char](1) NOT NULL,
	[numeroelectronico] [varchar](50) NULL,
	[estransgratuita] [char](2) NULL,
	[enviado_email] [char](2) NULL,
	[enviado_externo] [char](2) NULL,
	[tipoguiaremision] [char](2) NULL,
	[guiaremision] [varchar](30) NULL,
	[tipodocotrodocref] [char](2) NULL,
	[otrodocumentoref] [varchar](30) NULL,
	[ordencompra] [varchar](20) NULL,
	[placavehiculo] [varchar](8) NULL,
	[montofise] [decimal](8, 2) NULL,
	[tiporegimen] [varchar](3) NULL,
	[codigobbsssujetodetrac] [varchar](20) NULL,
	[numctabconacion] [varchar](30) NULL,
	[bientransfamazonia] [char](1) NULL,
	[servitransfamazonia] [char](1) NULL,
	[contratoconstamazonia] [char](1) NULL,
	[nombrexml] [varchar](100) NULL,
	[valorresumen] [varchar](100) NULL,
	[valorfirma] [varchar](max) NULL,
	[xmlgenerado] [varchar](max) NULL,
	[cod_aux_01] [varchar](10) NULL,
	[text_aux_01] [varchar](500) NULL,
	[cod_aux_02] [varchar](10) NULL,
	[text_aux_02] [varchar](500) NULL,
	[cod_aux_03] [varchar](10) NULL,
	[text_aux_03] [varchar](500) NULL,
	[cod_aux_04] [varchar](10) NULL,
	[text_aux_04] [varchar](500) NULL,
	[cod_aux_05] [varchar](10) NULL,
	[text_aux_05] [varchar](500) NULL,
	[cod_aux_06] [varchar](10) NULL,
	[text_aux_06] [varchar](500) NULL,
	[cod_aux_07] [varchar](10) NULL,
	[text_aux_07] [varchar](500) NULL,
	[cod_aux_08] [varchar](10) NULL,
	[text_aux_08] [varchar](500) NULL,
	[cod_aux_09] [varchar](10) NULL,
	[text_aux_09] [varchar](500) NULL,
	[cod_aux_10] [varchar](10) NULL,
	[text_aux_10] [varchar](500) NULL,
	[cod_aux_11] [varchar](10) NULL,
	[text_aux_11] [varchar](500) NULL,
	[usureg] [varchar](20) NULL,
	[fecreg] [datetime] NULL,
	[formapago] [varchar](255) NULL,
	[jsonpagocredito] [varchar](max) NULL,
	[hasRetencionIgv] [char](2) NOT NULL,
	[porcRetIgv] [float] NULL,
	[impRetIgv] [decimal](12, 2) NULL,
	[impOpeRetIgv] [decimal](12, 2) NULL,
 CONSTRAINT [PK_documento] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[empresa_id] ASC,
	[tipodocumento_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[documento]  WITH CHECK ADD  CONSTRAINT [FK_documento_cliente_tipodoc] FOREIGN KEY([cliente_tipodoc])
REFERENCES [dbo].[docpersona] ([codigosunat])
GO
ALTER TABLE [dbo].[documento] CHECK CONSTRAINT [FK_documento_cliente_tipodoc]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id del documento propio de la bd de la empresa cliente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de la empresa registrada en la tabla empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'empresa_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Relacionado con tabla tipodocumento o con tabla codigosunat codtabla=001' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'tipodocumento_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'serie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'serie_comprobante'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'correlativo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'nro_comprobante'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo tipo nota credito (tabla codigosunat codtabla=''009'')
codigo tipo nota debito (tabla codigosunat codtabla=''010'')
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'tiponotacreddeb'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo de Documento del cliente - Relacionado con tabla codigosunat codtabla = 006' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'cliente_tipodoc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nro de documento del cliente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'cliente_nrodoc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion del cliente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'cliente_nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'direccion de domicilio del cliente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'cliente_direccion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'correo electronico del cliente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'cliente_email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de emision del documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'fecha'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha vencimiento del documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'fechavencimientopago'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de moneda del documento PEN - USD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'moneda'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo de cambio del documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'tipocambio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descuento en porcentaje (sera aplicado sobre la suma de valorventa del detalledocumento)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'descuento'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'(suma de igv del detalledocumento) - (suma de igv del detalledocumento)*descuento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'igvventa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'(suma de isc del detalledocumento)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'iscventa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'otros tributos considerados en el documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'otrostributos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'otros cargos considerados en el documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'otroscargos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'= totalgravadas + totalexoneradas + igvventa + iscventa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'importeventa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'porcentaje de la percepcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'porcentpercep'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'importe de la percepcion (validar si va en moneda nacional o en moneda del documento)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'importepercep'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'porcentaje de la detraccion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'porcentdetrac'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'importe de la detraccion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'importedetrac'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'= importeventa (aplicando percepcion retencion y detraccion) || tener en cuenta que la percepcion puede que vaya siempre en moneda nacional -- validar' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'importefinal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'N - Normal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P-Pendiente | R-Rechazado | A-Aceptado | E-Error' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'situacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SI/NO --- Valida SI la operacion es gratuita o No lo es' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'estransgratuita'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SI -- Indica si el documento fue enviado por correo a la empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'enviado_email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SI -- Indica si el documento fue enviado por correo al cliente de la empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'enviado_externo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Guias de Remision relacionada al documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'guiaremision'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo de documento de algun otro documento relacionado al documento de venta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'tipodocotrodocref'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero del documento de algun otro documento relacionado al documento de venta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'otrodocumentoref'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo de bien o servicio sujeto a detraccion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'codigobbsssujetodetrac'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero de cta en el bco de la nacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'numctabconacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 indica q es un bien trasferido amazonia - leyenda' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'bientransfamazonia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 indica q es un servicio tranferido amazonia - leyenda' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'servitransfamazonia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 indica q es contrato de construccion amazonia - leyenda' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'documento', @level2type=N'COLUMN',@level2name=N'contratoconstamazonia'
GO

-- ----------------------------
--  Table structure for `detalledocumento`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[detalledocumento](
	[documento_id] [varchar](15) NOT NULL,
	[empresa_id] [int] NOT NULL,
	[tipodocumento_id] [varchar](2) NOT NULL,
	[line_id] [int] NOT NULL,
	[codigoproducto] [varchar](50) NULL,
	[codigosunat] [varchar](50) NULL,
	[nombreproducto] [varchar](500) NOT NULL,
	[afectoigv] [char](2) NOT NULL,
	[afectoisc] [char](2) NULL,
	[tipoafectacionigv] [char](2) NOT NULL,
	[tipocalculoisc] [char](2) NULL,
	[unidad] [varchar](5) NOT NULL,
	[cantidad] [float] NOT NULL,
	[preciounitario] [float] NOT NULL,
	[precioreferencial] [float] NULL,
	[valorunitario] [float] NOT NULL,
	[valorbruto] [float] NOT NULL,
	[valordscto] [float] NULL,
	[valorcargo] [float] NULL,
	[valorventa] [float] NULL,
	[isc] [float] NULL,
	[igv] [float] NOT NULL,
	[total] [float] NOT NULL,
	[factorigv] [float] NULL,
	[factorisc] [float] NULL,
	[tbvt_puntoorigen] [varchar](100) NULL,
	[tbvt_descripcionorigen] [varchar](100) NULL,
	[tbvt_puntodestino] [varchar](100) NULL,
	[tbvt_descripciondestino] [varchar](100) NULL,
	[tbvt_detalleviaje] [varchar](100) NULL,
	[tbvt_valorrefpreliminar] [varchar](100) NULL,
	[tbvt_valorrefcargaefectiva] [varchar](100) NULL,
	[tbvt_valorrefcargautil] [varchar](100) NULL,
 CONSTRAINT [PK_detalledocumento] PRIMARY KEY CLUSTERED 
(
	[documento_id] ASC,
	[empresa_id] ASC,
	[tipodocumento_id] ASC,
	[line_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[detalledocumento] ADD  CONSTRAINT [DF_detalledocumento_precioreferencial]  DEFAULT ((0.00)) FOR [precioreferencial]
GO
ALTER TABLE [dbo].[detalledocumento]  WITH CHECK ADD  CONSTRAINT [FK_detalledocumento_documento] FOREIGN KEY([documento_id], [empresa_id], [tipodocumento_id])
REFERENCES [dbo].[documento] ([id], [empresa_id], [tipodocumento_id])
GO
ALTER TABLE [dbo].[detalledocumento] CHECK CONSTRAINT [FK_detalledocumento_documento]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id de la tabla documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'documento_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'empresa_id de la tabla documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'empresa_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipodocumento_id de la tabla documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'tipodocumento_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nro de linea del detalle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'line_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo del producto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'codigoproducto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion del producto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'nombreproducto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SI/NO -- Indica SI es afecto a IGV o NO lo es' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'afectoigv'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SI/NO -- Indica SI es afecto a ISC o NO lo es' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'afectoisc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Relacionado con la tabla tipoafectacionigv' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'tipoafectacionigv'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Relacionado con la tabla tipocalculoisc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'tipocalculoisc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'revisar catalogo de unidades de sunat' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'unidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'cantidad de unidades' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'cantidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'precio unitario de venta (+ igv - descuento por unidad)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'preciounitario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'precio unitario del producto (+ igv)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'precioreferencial'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'precio del producto sin impuestos (sin igv, sin isc)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'valorunitario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'cantidad * valorunitario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'valorbruto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valor del descuento en moneda' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'valordscto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valorbruto - valordscto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'valorventa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'monto del impuesto selectivo al consumo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'isc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'(valorventa + isc)*(0.18) ---- valorigv=0.18' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'igv'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valorventa + isc + igv' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'detalledocumento', @level2type=N'COLUMN',@level2name=N'total'
GO

-- ----------------------------
--  Table structure for `docanticipo`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[docanticipo](
	[documento_id] [varchar](15) NOT NULL,
	[empresa_id] [int] NOT NULL,
	[tipodocumento_id] [varchar](2) NOT NULL,
	[line_id] [int] NOT NULL,
	[tipodocanticipo] [varchar](2) NOT NULL,
	[nrodocanticipo] [varchar](20) NOT NULL,
	[montoanticipo] [float] NOT NULL,
	[tipodocemisor] [varchar](3) NOT NULL,
	[nrodocemisor] [varchar](15) NOT NULL,
 CONSTRAINT [PK_docanticipo] PRIMARY KEY CLUSTERED 
(
	[documento_id] ASC,
	[empresa_id] ASC,
	[tipodocumento_id] ASC,
	[line_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


-- ----------------------------
--  Table structure for `docrefeguia`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[docrefeguia](
	[documento_id] [varchar](15) NOT NULL,
	[empresa_id] [int] NOT NULL,
	[tipodocumento_id] [varchar](2) NOT NULL,
	[tipo_guia] [varchar](2) NOT NULL,
	[numero_guia] [varchar](15) NOT NULL
) ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for `docreferencia`
-- ----------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[docreferencia](
	[empresa_id] [int] NOT NULL,
	[documento_id] [varchar](15) NOT NULL,
	[tipodocumento_id] [varchar](2) NOT NULL,
	[documentoref_id] [varchar](15) NOT NULL,
	[tipodocumentoref_id] [varchar](2) NOT NULL,
	[numerodocreferencia] [varchar](20) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[docreferencia]  WITH CHECK ADD  CONSTRAINT [FK_docreferencia_documento] FOREIGN KEY([documento_id], [empresa_id], [tipodocumento_id])
REFERENCES [dbo].[documento] ([id], [empresa_id], [tipodocumento_id])
GO
ALTER TABLE [dbo].[docreferencia] CHECK CONSTRAINT [FK_docreferencia_documento]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Formato ANNN-NNNNNNNN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'docreferencia', @level2type=N'COLUMN',@level2name=N'numerodocreferencia'
GO


-- ----------------------------
--  Table structure for `facturaguia`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[facturaguia](
	[documento_id] [varchar](15) NOT NULL,
	[empresa_id] [int] NOT NULL,
	[tipodocumento_id] [varchar](2) NOT NULL,
	[partida_direccion] [varchar](100) NULL,
	[partida_urbanizacion] [varchar](25) NULL,
	[partida_departamento] [varchar](30) NULL,
	[partida_provincia] [varchar](30) NULL,
	[partida_distrito] [varchar](30) NULL,
	[partida_pais] [char](2) NULL,
	[llegada_direccion] [varchar](100) NULL,
	[llegada_urbanizacion] [varchar](25) NULL,
	[llegada_departamento] [varchar](30) NULL,
	[llegada_provincia] [varchar](30) NULL,
	[llegada_distrito] [varchar](30) NULL,
	[llegada_pais] [char](2) NULL,
	[vehiculo_placa] [varchar](8) NULL,
	[vehiculo_constancia] [varchar](30) NULL,
	[vehiculo_marca] [varchar](30) NULL,
	[licencia_conducir] [varchar](16) NULL,
	[transportista_ruc] [varchar](11) NULL,
	[transportista_razonsocial] [varchar](100) NULL,
	[modalidad_transporte] [char](2) NULL,
	[peso_unidad] [varchar](3) NULL,
	[peso_cantidad] [decimal](15, 2) NULL,
 CONSTRAINT [PK_factura_guia] PRIMARY KEY CLUSTERED 
(
	[documento_id] ASC,
	[empresa_id] ASC,
	[tipodocumento_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


-- ----------------------------
--  Table structure for `tipodocumento`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tipodocumento](
	[id] [varchar](2) NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	[consultalibre] [char](1) NOT NULL,
 CONSTRAINT [PK_tipodocumento_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for `upais`
-- ----------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[upais](
	[id] [varchar](2) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
 CONSTRAINT [PK_upais] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



-- ----------------------------
--  Table structure for `udepartamentos`
-- ----------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[udepartamentos](
	[id] [varchar](6) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
 CONSTRAINT [PK_udepartamentos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


-- ----------------------------
--  Table structure for `uprovincias`
-- ----------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[uprovincias](
	[id] [varchar](6) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
	[iddep] [varchar](6) NOT NULL,
 CONSTRAINT [PK_uprovincias] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[uprovincias]  WITH CHECK ADD  CONSTRAINT [FK_uprovincias_udepartamentos] FOREIGN KEY([iddep])
REFERENCES [dbo].[udepartamentos] ([id])
GO
ALTER TABLE [dbo].[uprovincias] CHECK CONSTRAINT [FK_uprovincias_udepartamentos]
GO




-- ----------------------------
--  Table structure for `udistritos`
-- ----------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[udistritos](
	[id] [varchar](6) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
	[idprov] [varchar](6) NOT NULL,
 CONSTRAINT [PK_udistritos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[udistritos]  WITH CHECK ADD  CONSTRAINT [FK_udistritos_uprovincias] FOREIGN KEY([idprov])
REFERENCES [dbo].[uprovincias] ([id])
GO
ALTER TABLE [dbo].[udistritos] CHECK CONSTRAINT [FK_udistritos_uprovincias]
GO









